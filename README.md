<h1 align="center">Welcome to AnsibleUbuntuDEV 👋</h1>
<p>
  <a href="https://twitter.com/VPecquerie" target="_blank">
    <img alt="Twitter: VPecquerie" src="https://img.shields.io/twitter/follow/VPecquerie.svg?style=social" />
  </a>
</p>

> An ansible configuration to setup an Ubuntu based development environment

### 🏠 [Homepage](https://www.vincent-p.fr/)

## Install

```sh
Safety review and run the install script (as root)
```

## Author

👤 **Vincent PECQUERIE <contact@vincent-p.fr>**

* Website: https://www.vincent-p.fr
* Twitter: [@VPecquerie](https://twitter.com/VPecquerie)
* Github: [@VPecquerie](https://github.com/VPecquerie)
* LinkedIn: [@vincent-pecquerie-a1b76185](https://linkedin.com/in/vincent-pecquerie-a1b76185)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!
Feel free to check [issues page](https://gitlab.com/vincent-pecquerie/projects/environment/ansible/ansible-ubuntu-dev/-/issues). 

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_